# Ferri

Protocol for sending and accepting serialized Java objects over TCP.

Uses public keys for authentication.

## Methods

* Key exchange

    1. The client sends its public key and asks the server for its public key.

    2. The server sends its public key to the client.

* Send object

    1. The client sends an ecrypted serialized Java object to the server

    2. The client may then wait the server for an answer

The semantics depend on the protocol built over this.
This protocol works as Layer 4.5 on the OSI model.

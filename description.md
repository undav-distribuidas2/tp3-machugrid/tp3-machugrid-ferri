## build 4 projects

* client:
    * send request to scheduler
    * query results from scheduler
    
* scheduler:
    * accept request from client
    * send partial calculation to node
    * query partial results from node
    * merge results
    * accept results query from scheduler
    
* node:
    * receive partial calculation from scheduler
    * store partial results
    * accept partial results query from scheduler
  
* ferri:
    * generic methods to send and accept authenticated messages with payload


## execution timeline

1. a node joins the grid

    1. a node sends its info to the scheduler
    2. the scheduler may accept or reject
    3. if accepted, the node waits for the scheduler to send a task

2. a client requests computation

    1. a client sends a program, execution parameters and data to be processed
    2. the scheduler may accept or reject
    3. if accepted, the client will need to query for the results until it's done

3. the scheduler sends partial data to the nodes

    1. for each node connected, the scheduler asks if the node can run this program
    2. the scheduler adds the nodes that accepted to a list
    3. if no node accepted, the scheduler will answer an error to the client
    4. if some nodes accepted, the scheduler partitions the data and sends that to the nodes
    5. the scheduler waits for the nodes to answer the results

4. the node computes the results

    1. the node runs the received program with the parameters for each register of data
    2. the node saves the partial results
    3. the node sends the partial results to the scheduler
    
5. the scheduler merges the results
    
    1. when the last partial results file is received, the scheduler merges them
    2. the scheduler waits for the client to query the results

6. the client queries the results

    1. the client sends request to query the resutls from the scheduler
    2. if the computation is finished, the scheduler sends the results


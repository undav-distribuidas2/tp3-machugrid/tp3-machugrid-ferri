package ar.edu.undav.machugrid.ferri;

public class SchedulerEnd extends SchedulerMessage {

	@Override
	public void visit(SchedulerMessageVisitor visitor) {
		visitor.end(this);
	}

}

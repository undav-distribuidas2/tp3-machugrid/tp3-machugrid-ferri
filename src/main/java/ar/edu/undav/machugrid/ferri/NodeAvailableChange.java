package ar.edu.undav.machugrid.ferri;

public class NodeAvailableChange extends NodeMessage {

	boolean isAvailable;
	
	
	public NodeAvailableChange(boolean isAvailable) {
		super();
		this.isAvailable = isAvailable;
	}

	@Override
	public void visit(NodeMessageVisitor visitor) {
		visitor.availableChange(this);
	}

}

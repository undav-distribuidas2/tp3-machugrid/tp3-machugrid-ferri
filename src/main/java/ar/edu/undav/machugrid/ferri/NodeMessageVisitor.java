package ar.edu.undav.machugrid.ferri;

/**
 * To 
 * @author ernok
 *
 */
public interface NodeMessageVisitor {
	
	void end(NodeEnd message);
	
	void register(NodeRegister message);
	
	void availableChange(NodeAvailableChange message);
	
	void result(NodeResult message);
	
}

package ar.edu.undav.machugrid.ferri;

import java.io.Serializable;

/**
 * Base class for messages sent by the node.
 * @author ernok
 *
 */
public abstract class NodeMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	public abstract void visit(NodeMessageVisitor visitor);
	
}


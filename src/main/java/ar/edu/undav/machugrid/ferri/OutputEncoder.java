package ar.edu.undav.machugrid.ferri;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.PublicKey;

import javax.crypto.Cipher;

/**
 * Encodes messages that will be sent.
 * @author ernok
 *
 */
public class OutputEncoder {

	private final String cipherAlgorithm; 
	private final PublicKey remotePublicKey;
	
	public OutputEncoder(String cipherAlgorithm, PublicKey remotePublicKey) throws InvalidKeyException {
		this.cipherAlgorithm = cipherAlgorithm;
		this.remotePublicKey = remotePublicKey;
	}

	public <T extends Serializable> byte[] encode(T obj) throws Exception {
		byte[] unencodedBytes;
		try (
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);

		) {
			oos.writeObject(obj);
			unencodedBytes = baos.toByteArray();
		}

		Cipher cipher = Cipher.getInstance(cipherAlgorithm);
		cipher.init(Cipher.ENCRYPT_MODE, remotePublicKey);
		return cipher.doFinal(unencodedBytes);
	}

}

package ar.edu.undav.machugrid.ferri;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.NoSuchPaddingException;

public class FerriKeyManager {

	private final String encryptionAlgorithm;
	
	public FerriKeyManager(String encryptionAlgorithm) {
		this.encryptionAlgorithm = encryptionAlgorithm;
	}
	
	
	public String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}


	/**
	 * Generate public and private key pair
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	public KeyPair generateKeyPair() throws NoSuchAlgorithmException {
		return KeyPairGenerator.getInstance(encryptionAlgorithm).generateKeyPair();
	}

	/**
	 * Send a message to the server to ask for its public key.
	 * 
	 * @param socket
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 */
	public PublicKey askPublicKey(Socket socket, PublicKey myPublicKey)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		
		sendPublicKey(socket, myPublicKey);
		return receivePublicKey(socket);
	}

	/**
	 * Receive public key from the other node.
	 * 
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public PublicKey receivePublicKey(Socket socket)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		
		InputStream is = socket.getInputStream();
		byte[] remotePublicKeyBytes = is.readAllBytes();
		return KeyFactory.getInstance(encryptionAlgorithm).generatePublic(new X509EncodedKeySpec(remotePublicKeyBytes));
	}

	/**
	 * Send a public key. Answer to public key request.
	 * @throws IOException 
	 */
	public void sendPublicKey(Socket socket, PublicKey myPublicKey) throws IOException {
		OutputStream os = socket.getOutputStream();
		os.write(myPublicKey.getEncoded());		
	}

}

package ar.edu.undav.machugrid.ferri;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.PublicKey;

public class FerriConnectionFactory {

	private FerriKeyManager ferriKeyManager;

	/**
	 * 
	 * @param ferriKeyManager Recommended "RSA"
	 */
	public FerriConnectionFactory(FerriKeyManager ferriKeyManager) {
		this.ferriKeyManager = ferriKeyManager;
	}

	/**
	 * Wait connections (as server)
	 * 
	 * @throws Exception
	 */
	public FerriConnection listen(ServerSocket serverSocket) throws Exception {
		Socket clientSocket = serverSocket.accept();

		// ask the remote public key
		PublicKey remotePublicKey = ferriKeyManager.receivePublicKey(clientSocket);

		// generate a key pair for this machine
		KeyPair myKeyPair = ferriKeyManager.generateKeyPair();

		// send my public key
		ferriKeyManager.sendPublicKey(clientSocket, myKeyPair.getPublic());

		// build a connection on the socket with the keys
		return buildConnection(clientSocket, myKeyPair, remotePublicKey);
	}

	/**
	 * Start a connection to the server (as client)
	 * 
	 * @param address
	 * @param port
	 * @return
	 * @throws IOException
	 */
	public FerriConnection connectToServer(Socket socket) {
		try {
			// generate a new key pair for this machine
			KeyPair myKeyPair = ferriKeyManager.generateKeyPair();
			
			// send my public key and receive the remote public key as the answer
			PublicKey remotePublicKey = ferriKeyManager.askPublicKey(socket, myKeyPair.getPublic());
			
			// build a connection on the socket, using the keys
			return buildConnection(socket, myKeyPair, remotePublicKey);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	FerriConnection buildConnection(Socket socket, KeyPair myKeyPair, PublicKey remotePublicKey) throws Exception {
		OutputEncoder encoder = new OutputEncoder(ferriKeyManager.getEncryptionAlgorithm(), remotePublicKey);
		InputDecoder decoder = new InputDecoder(ferriKeyManager.getEncryptionAlgorithm(), myKeyPair.getPrivate());
		
		return new FerriConnection(socket, encoder, decoder);
	}

}
package ar.edu.undav.machugrid.ferri;

import java.io.Serializable;
import java.net.Socket;

/**
 * A wrapper on a socket to send and receive ferri messages.
 * 
 * @author ernok
 *
 */
public class FerriConnection implements AutoCloseable {

	private Socket socket;
	private OutputEncoder encoder;
	private InputDecoder decoder;

	/**
	 * Build a FerriConnection from an existing socket.
	 * 
	 * @param socket
	 *            Socket to communicate with the other node.
	 * @param encoder
	 *            Encodes messages that will be sent.
	 * @param decoder
	 *            Decodes messages received.
	 */
	FerriConnection(Socket socket, OutputEncoder encoder, InputDecoder decoder) {
		this.socket = socket;
		this.encoder = encoder;
		this.decoder = decoder;
	}

	public void close() throws Exception {
		socket.close();
	}

	public <T extends Serializable> void send(T message) throws Exception {
		byte[] encryptedBytes = encoder.encode(message);
		socket.getOutputStream().write(encryptedBytes);
	}

	public <T extends Serializable> T receive(Class<T> clazz) throws Exception {
		byte[] encryptedBytes = socket.getInputStream().readAllBytes();
		return decoder.decode(clazz, encryptedBytes);
	}

}
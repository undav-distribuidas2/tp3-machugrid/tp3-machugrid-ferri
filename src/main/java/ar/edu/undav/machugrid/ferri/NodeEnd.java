package ar.edu.undav.machugrid.ferri;

public class NodeEnd extends NodeMessage {

	@Override
	public void visit(NodeMessageVisitor visitor) {
		visitor.end(this);
	}

}

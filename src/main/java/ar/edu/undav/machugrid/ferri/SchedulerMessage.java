package ar.edu.undav.machugrid.ferri;

import java.io.Serializable;

/**
 * Base class for messages sent by the scheduler.
 * @author ernok
 *
 */
public abstract class SchedulerMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	public abstract void visit(SchedulerMessageVisitor visitor);
	
}


package ar.edu.undav.machugrid.ferri;

public class SchedulerExec extends SchedulerMessage {

	@Override
	public void visit(SchedulerMessageVisitor visitor) {
		visitor.exec(this);
	}

}

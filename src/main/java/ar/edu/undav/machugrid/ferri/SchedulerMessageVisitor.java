package ar.edu.undav.machugrid.ferri;

/**
 * 
 * @author ernok
 *
 */
public interface SchedulerMessageVisitor {
	
	void end(SchedulerEnd message);
	
	void exec(SchedulerExec message);
	
}

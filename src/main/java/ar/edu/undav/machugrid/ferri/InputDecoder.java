package ar.edu.undav.machugrid.ferri;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.security.InvalidKeyException;
import java.security.PrivateKey;

import javax.crypto.Cipher;

/**
 * Decode messages received.
 * @author ernok
 *
 */
public class InputDecoder {

	private final String cipherAlgorithm;
	private final PrivateKey myPrivateKey;

	public InputDecoder(String cipherAlgorithm, PrivateKey myPrivateKey) throws InvalidKeyException {
		super();
		this.cipherAlgorithm = cipherAlgorithm;
		this.myPrivateKey = myPrivateKey;
	}

	@SuppressWarnings("unchecked")
	public <T> T decode(Class<T> clazz, byte[] encodedBytes) throws Exception {
		Cipher cipher = Cipher.getInstance(cipherAlgorithm);
		cipher.init(Cipher.DECRYPT_MODE, myPrivateKey);
		byte[] decodedBytes = cipher.doFinal(encodedBytes);

		try (
				ByteArrayInputStream bais = new ByteArrayInputStream(decodedBytes);
				ObjectInputStream ois = new ObjectInputStream(bais);

		) {
			return (T) ois.readObject();
		}
	}

}

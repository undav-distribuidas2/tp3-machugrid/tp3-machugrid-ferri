package ar.edu.undav.machugrid.ferri;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;

public class NodeRegisterTest {

	@Test
	public void test_visit() {
		NodeRegister message = new NodeRegister();
		
		NodeMessageVisitor mockVisitor = mock(NodeMessageVisitor.class);
		
		message.visit(mockVisitor);
		
		verify(mockVisitor).register(same(message));
		verifyNoMoreInteractions(mockVisitor);
	}
	
}

package ar.edu.undav.machugrid.ferri;

import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FerriConnectionFactoryTest {

	@InjectMocks
	private FerriConnectionFactory ferriConnectionFactory;

	@Mock
	private FerriKeyManager ferriKeyManager;

	@Test
	public void test_listen() throws Exception {
		FerriConnectionFactory spy = spy(ferriConnectionFactory);

		ServerSocket serverSocket = mock(ServerSocket.class);

		Socket clientSocket = mock(Socket.class);
		doReturn(clientSocket).when(serverSocket).accept();

		PublicKey remotePublicKey = mock(PublicKey.class);
		PublicKey myPublicKey = mock(PublicKey.class);
		PrivateKey myPrivateKey = mock(PrivateKey.class);
		KeyPair myKeyPair = new KeyPair(myPublicKey, myPrivateKey);
		doReturn(remotePublicKey).when(ferriKeyManager).receivePublicKey(same(clientSocket));
		doReturn(myKeyPair).when(ferriKeyManager).generateKeyPair();

		FerriConnection mockResult = mock(FerriConnection.class);
		doReturn(mockResult).when(spy).buildConnection(same(clientSocket), same(myKeyPair), same(remotePublicKey));

		FerriConnection result = spy.listen(serverSocket);

		verify(ferriKeyManager).receivePublicKey(same(clientSocket));
		verify(ferriKeyManager).generateKeyPair();
		verify(ferriKeyManager).sendPublicKey(same(clientSocket), same(myKeyPair.getPublic()));
		verifyNoMoreInteractions(ferriKeyManager);

		assertSame(mockResult, result);
	}

	@Test
	public void test_connectToServer() throws Exception {
		FerriConnectionFactory spy = spy(ferriConnectionFactory);

		Socket socket = mock(Socket.class);

		PublicKey myPublicKey = mock(PublicKey.class);
		PrivateKey myPrivateKey = mock(PrivateKey.class);
		KeyPair myKeyPair = new KeyPair(myPublicKey, myPrivateKey);
		PublicKey remotePublicKey = mock(PublicKey.class);
		doReturn(myKeyPair).when(ferriKeyManager).generateKeyPair();
		doReturn(remotePublicKey).when(ferriKeyManager).askPublicKey(same(socket), same(myPublicKey));

		FerriConnection mockResult = mock(FerriConnection.class);
		doReturn(mockResult).when(spy).buildConnection(same(socket), same(myKeyPair), same(remotePublicKey));

		FerriConnection result = spy.connectToServer(socket);

		verify(ferriKeyManager).generateKeyPair();
		verify(ferriKeyManager).askPublicKey(same(socket), same(myPublicKey));
		verifyNoMoreInteractions(ferriKeyManager);

		assertSame(mockResult, result);
	}

	@Test
	public void test_connectToServer_error() throws Exception {
		FerriConnectionFactory spy = spy(ferriConnectionFactory);

		Socket socket = mock(Socket.class);

		doThrow(new RuntimeException()).when(ferriKeyManager).generateKeyPair();

		FerriConnection result = spy.connectToServer(socket);

		verify(ferriKeyManager).generateKeyPair();
		verifyNoMoreInteractions(ferriKeyManager);

		assertNull(result);
	}

	@Test
	public void test_buildConnection() throws Exception {
		assertNotNull(ferriConnectionFactory.buildConnection(
				mock(Socket.class),
				new KeyPair(mock(PublicKey.class), mock(PrivateKey.class)),
				mock(PublicKey.class)));
	}
}

package ar.edu.undav.machugrid.ferri;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

import org.junit.Test;

public class OutputEncoderTest {

	private final String cipherAlgorithm;
	private final PublicKey publicKey;
	private final PrivateKey privateKey;

	private final OutputEncoder encoder;

	public OutputEncoderTest() throws Exception {
		cipherAlgorithm = "RSA";

		KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
		publicKey = keyPair.getPublic();
		privateKey = keyPair.getPrivate();

		encoder = new OutputEncoder(cipherAlgorithm, publicKey);
	}

	@Test
	public void test_encode() throws Exception {
		String message = "my message xoxo";

		byte[] encodedBytes = encoder.encode(message);
		
		String decoded;
		{
			Cipher cipher = Cipher.getInstance(cipherAlgorithm);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decodedBytes = cipher.doFinal(encodedBytes);
			
			try (
					ByteArrayInputStream bais = new ByteArrayInputStream(decodedBytes);
					ObjectInputStream ois = new ObjectInputStream(bais);

			) {
				decoded = (String) ois.readObject();
			}
		}
		
		assertEquals(message, decoded);
	}
}

package ar.edu.undav.machugrid.ferri;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;

public class SchedulerEndTest {

	@Test
	public void test_visit() {
		SchedulerEnd message = new SchedulerEnd();
		
		SchedulerMessageVisitor mockVisitor = mock(SchedulerMessageVisitor.class);
		
		message.visit(mockVisitor);
		
		verify(mockVisitor).end(same(message));
		verifyNoMoreInteractions(mockVisitor);
	}
	
}

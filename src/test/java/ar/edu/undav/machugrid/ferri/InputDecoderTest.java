package ar.edu.undav.machugrid.ferri;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

import org.junit.Test;

public class InputDecoderTest {

	private final String cipherAlgorithm;
	private final PublicKey publicKey;
	private final PrivateKey privateKey;

	private final InputDecoder decoder;

	public InputDecoderTest() throws Exception {
		cipherAlgorithm = "RSA";

		KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
		publicKey = keyPair.getPublic();
		privateKey = keyPair.getPrivate();

		decoder = new InputDecoder(cipherAlgorithm, privateKey);
	}

	@Test
	public void test_decode() throws Exception {
		String message = "my message";
		
		byte[] unencodedBytes;
		try (
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);

		) {
			oos.writeObject(message);
			unencodedBytes = baos.toByteArray();
		}
		
		byte[] encodedBytes;
		{
			Cipher cipher = Cipher.getInstance(cipherAlgorithm);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			encodedBytes = cipher.doFinal(unencodedBytes);
		}

		String decoded = decoder.decode(String.class, encodedBytes);
		
		assertEquals(message, decoded);
	}
}

package ar.edu.undav.machugrid.ferri;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FerriConnectionTest {

	@InjectMocks
	private FerriConnection ferriConnection;

	@Mock
	private Socket socket;

	@Mock
	private OutputEncoder encoder;

	@Mock
	private InputDecoder decoder;

	@Test
	public void test_send() throws Exception {
		String message = "my message";
		byte[] encodedBytes = "supose this is encoded".getBytes();
		
		OutputStream os = mock(OutputStream.class);
		doReturn(os).when(socket).getOutputStream();
		doReturn(encodedBytes).when(encoder).encode(anyString());
		
		ferriConnection.send(message);

		verify(socket).getOutputStream();
		verifyNoMoreInteractions(socket);

		verify(os).write(same(encodedBytes));
		verifyNoMoreInteractions(os);
		
		verify(encoder).encode(same(message));
		verifyNoMoreInteractions(encoder);
	}

	@Test
	public void test_receive() throws Exception {
		byte[] encodedBytes = "supose this is encoded".getBytes();
		String message = "my message";
		
		InputStream is = mock(InputStream.class);
		doReturn(encodedBytes).when(is).readAllBytes();
		doReturn(is).when(socket).getInputStream();
		doReturn(message).when(decoder).decode(eq(String.class), same(encodedBytes));
		
		String received = ferriConnection.receive(String.class);

		verify(socket).getInputStream();
		verifyNoMoreInteractions(socket);

		verify(is).readAllBytes();
		verifyNoMoreInteractions(is);

		verify(decoder).decode(eq(String.class), same(encodedBytes));
		verifyNoMoreInteractions(decoder);
		
		assertEquals(message, received);
	}
	
	@Test
	public void test_close() throws Exception {
		ferriConnection.close();
		
		verify(socket).close();
		verifyNoMoreInteractions(socket);
	}
	
}

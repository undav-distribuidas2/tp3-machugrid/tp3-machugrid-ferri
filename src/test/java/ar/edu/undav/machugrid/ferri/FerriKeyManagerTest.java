package ar.edu.undav.machugrid.ferri;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import org.junit.Test;

public class FerriKeyManagerTest {
	
	private FerriKeyManager ferriKeyManager = new FerriKeyManager("RSA");
	
	
	@Test
	public void test_generateKeyPair() throws NoSuchAlgorithmException {
		KeyPair result = ferriKeyManager.generateKeyPair();
		assertNotNull(result);
	}
	
	@Test
	public void test_askPublicKey() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		FerriKeyManager spy = spy(ferriKeyManager);
		
		PublicKey mockPublicKey = mock(PublicKey.class);
		Socket mockSocket = mock(Socket.class);
		PublicKey mockResult = mock(PublicKey.class);
		
		doReturn(mockPublicKey).when(spy).receivePublicKey(same(mockSocket));
		doNothing().when(spy).sendPublicKey(any(Socket.class), any(PublicKey.class));
		doReturn(mockResult).when(spy).receivePublicKey(any(Socket.class));
		
		PublicKey result = spy.askPublicKey(mockSocket, mockPublicKey);
		
		verify(spy).askPublicKey(same(mockSocket), same(mockPublicKey));
		verify(spy).sendPublicKey(same(mockSocket), same(mockPublicKey));
		verify(spy).receivePublicKey(same(mockSocket));
		verifyNoMoreInteractions(spy);
		
		assertSame(mockResult, result);
	}

	@Test
	public void test_receivePublicKey() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
		PublicKey publicKey = keyPair.getPublic(); 
		
		Socket mockSocket = mock(Socket.class);
		InputStream mockIs = mock(InputStream.class);
		byte[] remotePublicKeyBytes = publicKey.getEncoded();
		
		doReturn(mockIs).when(mockSocket).getInputStream();
		doReturn(remotePublicKeyBytes).when(mockIs).readAllBytes();
		
		PublicKey result = ferriKeyManager.receivePublicKey(mockSocket);
		
		verify(mockSocket).getInputStream();
		verifyNoMoreInteractions(mockSocket);
		
		assertEquals(publicKey, result);
	}

	@Test
	public void test_sendPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
		KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
		PublicKey publicKey = keyPair.getPublic(); 
		
		Socket mockSocket = mock(Socket.class);
		OutputStream mockOs = mock(OutputStream.class);
		
		doReturn(mockOs).when(mockSocket).getOutputStream();
		
		ferriKeyManager.sendPublicKey(mockSocket, publicKey);
		
		verify(mockSocket).getOutputStream();
		verifyNoMoreInteractions(mockSocket);
		
		byte[] expectedBytes = publicKey.getEncoded();
		verify(mockOs).write(eq(expectedBytes ));
	}
	
	@Test
	public void test_getEncryptionAlgorithm() {
		assertEquals("RSA", ferriKeyManager.getEncryptionAlgorithm());
	}
	
}
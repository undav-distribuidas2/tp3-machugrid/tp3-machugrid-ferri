package ar.edu.undav.machugrid.ferri;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;

public class NodeEndTest {

	@Test
	public void test_visit() {
		NodeEnd message = new NodeEnd();
		
		NodeMessageVisitor mockVisitor = mock(NodeMessageVisitor.class);
		
		message.visit(mockVisitor);
		
		verify(mockVisitor).end(same(message));
		verifyNoMoreInteractions(mockVisitor);
	}
	
}
